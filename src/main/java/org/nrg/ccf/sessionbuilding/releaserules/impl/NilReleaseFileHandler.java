package org.nrg.ccf.sessionbuilding.releaserules.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.nrg.ccf.sessionbuilding.abst.AbstractCcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.exception.ReleaseTransferException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

@CcfReleaseFileHandler(description = "Nil File Handler - Doesn't transfer file resources.")
public class NilReleaseFileHandler extends AbstractCcfReleaseFileHandler {

	static Logger logger = Logger.getLogger(NilReleaseFileHandler.class);
	
	public NilReleaseFileHandler(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession, CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, destSession, releaseRules, user);
	}

	@Override
	public void transferFiles() throws ReleaseTransferException {
		logger.debug("NilReleaseFileHandler called.  No resources will be transferred.");
	}

}
