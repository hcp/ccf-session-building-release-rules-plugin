package org.nrg.ccf.sessionbuilding.releaserules.impl;

import java.util.List;

import org.nrg.ccf.sessionbuilding.abst.AbstractUnprocResourceGenerator;
import org.nrg.ccf.sessionbuilding.anno.CcfResourceGenerator;
import org.nrg.ccf.sessionbuilding.exception.ReleaseResourceException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.pojo.ResourceValidationResults;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CcfResourceGenerator(description = "Nil Resource Generator - Creates no resources")
public class NilResourceGenerator extends AbstractUnprocResourceGenerator {

	public NilResourceGenerator(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata combSession, CcfReleaseRulesI releaseRules, UserI user) {
		super(srcSessions, combSession, releaseRules, user);
	}

	@Override
	public void generateResources() throws ReleaseResourceException {
		log.debug("Nil Resource Generator called.  No resources will be generated. (SESSION=" + getSession().getLabel() + ")");
	}

	@Override
	public ResourceValidationResults validateUnprocResources() throws ReleaseResourceException {
		return ResourceValidationResults.newValidInstance();
	}

}
