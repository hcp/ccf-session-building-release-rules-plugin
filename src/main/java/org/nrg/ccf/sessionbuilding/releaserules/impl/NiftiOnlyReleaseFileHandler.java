package org.nrg.ccf.sessionbuilding.releaserules.impl;

import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sessionbuilding.abst.AbstractCcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.anno.CcfReleaseFileHandler;
import org.nrg.ccf.sessionbuilding.exception.ReleaseTransferException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.interfaces.FilepathTransformerI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CcfReleaseFileHandler(description = "NIFTI File Handler - Transfers NIFTI Resources")
public class NiftiOnlyReleaseFileHandler extends AbstractCcfReleaseFileHandler {
	
	private FilepathTransformerI _transformer;

	public NiftiOnlyReleaseFileHandler(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession, 
			CcfReleaseRulesI releaseRules, FilepathTransformerI transformer, UserI user) {
		super(srcSessions, destSession, releaseRules, user);
		_transformer = transformer;
	}

	public String SOURCE_SESSION_NIFTI_RESOURCE = CommonConstants.NIFTI_RESOURCE;
	public String DEST_SESSION_NIFTI_RESOURCE = CommonConstants.NIFTI_RESOURCE;
	public static final String EVENT_ACTION = "Transferred files to session";

	@Override
	public void transferFiles() throws ReleaseTransferException {
		final PersistentWorkflowI wrk;
		try {
			log.debug("Nifti-Only File Handler called.  Transferring NIFTI resources (SESSION=" + getDestSession().getLabel() + ").");
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getUser(), getDestSession().getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EVENT_ACTION, EVENT_ACTION, null));
			final EventMetaI ci = wrk.buildEvent();
			transferFilesForResource(SOURCE_SESSION_NIFTI_RESOURCE, DEST_SESSION_NIFTI_RESOURCE, null, _transformer, ci);
		} catch (Exception e) {
			log.error("Exception thrown during file transfer (" + e.toString() + ").");
			throw new ReleaseTransferException("ERROR:  Could not transfer files (SESSION=" + getDestSession().getLabel() + ")", e);
		}
	}

}
