package org.nrg.ccf.sessionbuilding.releaserules.transformers;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nrg.ccf.sessionbuilding.interfaces.FilepathTransformerI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;

public class DefaultSeriesDescAndSessionLabelTransformer implements FilepathTransformerI {

	@Override
	public String transformParentDirectoryPath(String fileDirectory, File sessionFile, XnatImagesessiondata srcSession, XnatImagesessiondata destSession,
			XnatImagescandataI srcSessScan, XnatImagescandata destSessScan) {
		return fileDirectory;
	}

	@Override
	public String transformFilename(String fileName, XnatImagesessiondata srcSession, XnatImagesessiondata destSession, XnatImagescandataI srcSessionScan,
			XnatImagescandata destSessionScan) {
		fileName = replaceAllowForTransform(fileName,"^" + srcSession.getLabel(), destSession.getLabel());
		fileName = replaceAllowForTransform(fileName,"_" + srcSessionScan.getSeriesDescription(), "_" + destSessionScan.getSeriesDescription());
		return fileName;
	}

	private String replaceAllowForTransform(String fileName, String srcStr, String destStr) {
		// Sometimes NIFTI doesn't get generated exactly according to the series description, but we
		// try to leave the original series description alone as much as possible
		final List<String> toTryList = Arrays.asList(new String[] { srcStr, srcStr.replace("__","_").replace(" ","")});
		for (final String toTry : toTryList) {
	        final Pattern pattern = Pattern.compile(toTry);
	        final Matcher matcher = pattern.matcher(fileName);
			if (matcher.find()) {
				return fileName.replaceFirst(toTry, destStr);
			}
		}
		// No replace here because srcStr is checked for matches above.  If found will be 
		// replaced and returned above.
		return fileName;
	}
	
}
