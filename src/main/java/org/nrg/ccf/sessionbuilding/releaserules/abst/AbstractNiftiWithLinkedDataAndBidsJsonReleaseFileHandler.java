package org.nrg.ccf.sessionbuilding.releaserules.abst;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.nrg.ccf.sessionbuilding.exception.ReleaseTransferException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.interfaces.FilepathTransformerI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;

public abstract class AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler extends AbstractNiftiWithLinkedDataReleaseFileHandler {
	
	protected final List<String> pathMatchRegex = new ArrayList<>();

	protected FilepathTransformerI _transformer;
	
	static Logger logger = Logger.getLogger(AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler.class);

	public AbstractNiftiWithLinkedDataAndBidsJsonReleaseFileHandler(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession, 
			CcfReleaseRulesI releaseRules, FilepathTransformerI transformer, UserI user) {
		super(srcSessions, destSession, releaseRules, transformer, user);
		_transformer = transformer;
	}

	@Override
	public void transferFiles() throws ReleaseTransferException {
		final PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getUser(), getDestSession().getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EVENT_ACTION, EVENT_ACTION, null));
			final EventMetaI ci = wrk.buildEvent();
			//List<String> noInitialFramesFilesRegex = new ArrayList<>();
			// Exclude InitialFrames files.
			//noInitialFramesFilesRegex.add("^((?!InitialFrames).)*$");
			//transferFilesForResourceWithHandlingForJsonFiles(SOURCE_SESSION_NIFTI_RESOURCE, DEST_SESSION_NIFTI_RESOURCE, true, true, noInitialFramesFilesRegex, ci);
			transferFilesForResourceWithHandlingForJsonFiles(SOURCE_SESSION_NIFTI_RESOURCE, DEST_SESSION_NIFTI_RESOURCE, null, _transformer, ci);
			transferFilesForResource(SOURCE_SESSION_LINKED_DATA_RESOURCE, DEST_SESSION_LINKED_DATA_RESOURCE, pathMatchRegex, _transformer, ci);
		} catch (Exception e) {
			throw new ReleaseTransferException("ERROR:  Could not transfer files", e);
		}
	}

	public FilepathTransformerI getTransformer() {
		return _transformer;
	}

	public List<String> getPathMatchRegex() {
		return pathMatchRegex;
	}

}
