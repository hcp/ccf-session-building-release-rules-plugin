package org.nrg.ccf.sessionbuilding.releaserules.abst;

import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.sessionbuilding.exception.ReleaseTransferException;
import org.nrg.ccf.sessionbuilding.interfaces.CcfReleaseRulesI;
import org.nrg.ccf.sessionbuilding.interfaces.FilepathTransformerI;
import org.nrg.ccf.sessionbuilding.releaserules.impl.NiftiOnlyReleaseFileHandler;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;

public abstract class AbstractNiftiWithLinkedDataReleaseFileHandler extends NiftiOnlyReleaseFileHandler {
	
	private final List<String> pathMatchRegex = new ArrayList<>();

	public String SOURCE_SESSION_LINKED_DATA_RESOURCE = CommonConstants.LINKED_DATA_RESOURCE;
	public String DEST_SESSION_LINKED_DATA_RESOURCE = CommonConstants.LINKED_DATA_RESOURCE;

	private FilepathTransformerI _transformer;

	public AbstractNiftiWithLinkedDataReleaseFileHandler(List<XnatImagesessiondata> srcSessions, XnatImagesessiondata destSession, 
			CcfReleaseRulesI releaseRules, FilepathTransformerI transformer, UserI user) {
		super(srcSessions, destSession, releaseRules, transformer, user);
		_transformer = transformer;
	}

	@Override
	public void transferFiles() throws ReleaseTransferException {
		final PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getUser(), getDestSession().getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EVENT_ACTION, EVENT_ACTION, null));
			final EventMetaI ci = wrk.buildEvent();
			transferFilesForResource(SOURCE_SESSION_NIFTI_RESOURCE, DEST_SESSION_NIFTI_RESOURCE, null, _transformer, ci);
			transferFilesForResource(SOURCE_SESSION_LINKED_DATA_RESOURCE, DEST_SESSION_LINKED_DATA_RESOURCE, pathMatchRegex, _transformer, ci);
		} catch (Exception e) {
			throw new ReleaseTransferException("ERROR:  Could not transfer files", e);
		}
	}

	public List<String> getPathMatchRegex() {
		return pathMatchRegex;
	}

}
